Oke, setelah diinstall, lanjut start apachenya pake command:

(btw ini perlu akun root atau akun yang punya privilege yang setara dengan dia)

`sudo systemctl start httpd`{{execute}}

**cek statusnya:**

`sudo systemctl status httpd`{{execute}}

Sekarang coba akses **Tab Dashboard** diatas terminal ini. Klik tombol refresh. Nah, dia akan muncul tampilan default homepagenya si Apache HTTP Server.
 
> kalo agan nyoba di PC masing masing, cara ngeceknya tinggal buka browser, lalu akses dengan http://ip_agan

Terus agan mau coba ubah tampilannya nih. Gimana cara mengubahnya? weh slow, pertama-tama jangan lupa open firewall di server kalian, atau kalo mau lebih gampang, matiin aja firewallnya. Caranya?

**kalo mau open firewall, pake command ini:**

> `sudo firewall-cmd --permanent --add-service=http`{{copy}}

> **jangan lupa di reload gan:**

> `sudo firewall-cmd --reload`{{copy}}

**kalo mau disable firewall, pake command ini:**

> `sudo systemctl stop firewalld`{{copy}}

> **cek statusnya gan:**

> `sudo systemctl status firewalld`{{execute}}

---

Hayuuk lanjut,

Sekarang kita coba ubah tampilan homepagenya, caranya? 

1. Pastiin agan punya text editor, contohnya kaya vim, **nano** dsb.

2. Nah untuk ubah homepagenya itu kita harus edit **index.html** yang ada di folder **/var/www/html**, kalo gak ada filenya, kita buat aja. Caranya? 

`sudo vi /var/www/html/index.html`{{copy}}

pencet **insert**, lalu paste salah satu contoh isi dari file html ini:

```
<html lang="en-US">

<head>
    <title>Try Katacoda</title>
</head>

<body>
<h1>hey hey hey</h1>
</body>
</html>
```

Pencet **Esc** lalu save dengan ketik **:x** lalu **Enter**

Setelah itu coba restart Apache HTTPnya pake command:

> `sudo systemctl restart httpd`{{execute}}

Lalu coba akses lagi web servernya di browser, dan hoopla, jadi deh.
